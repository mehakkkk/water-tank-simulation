var canvas1 = document.getElementById("mycanvas");
var context1 = canvas1.getContext("2d");
var value1;
var value2;
//draw 1 tank
start();
function tank1() {
    var p1 = new Geometry.Point(520, 420);
    var rect1 = new Geometry.Rectangle(context1, 170, 120, p1);
    rect1.draw();
    //draw small square
    var p2 = new Geometry.Point(500, 500);
    var rect2 = new Geometry.Rectangle(context1, 20, 20, p2);
    rect2.draw();
    //draw line
    var p3 = new Geometry.Point(500, 510);
    var p4 = new Geometry.Point(420, 510);
    var line1 = new Geometry.Line(p3, p4, context1);
    line1.draw("black");
    //draw triangle using line class
    var p5 = new Geometry.Point(420, 490);
    var p6 = new Geometry.Point(420, 530);
    var line2 = new Geometry.Line(p5, p6, context1);
    line2.draw("black");
    var p7 = new Geometry.Point(420, 490);
    var p8 = new Geometry.Point(380, 510);
    var line3 = new Geometry.Line(p7, p8, context1);
    line3.draw("black");
    var p9 = new Geometry.Point(420, 530);
    var p10 = new Geometry.Point(380, 510);
    var line4 = new Geometry.Line(p9, p10, context1);
    line4.draw("black");
    //draw circle
    var c1 = new Geometry.Point(375, 510);
    var a1 = new Geometry.Point(360, 310);
    var C2 = new Geometry.circle(c1, a1, 5, context1);
    C2.draw();
    //draw 2 bending lines
    var p11 = new Geometry.Point(370, 510);
    var p12 = new Geometry.Point(310, 510);
    var line5 = new Geometry.Line(p11, p12, context1);
    line5.draw("grey");
    var p13 = new Geometry.Point(310, 510);
    var p14 = new Geometry.Point(310, 350);
    var line6 = new Geometry.Line(p13, p14, context1);
    line6.draw("grey");
    var p15 = new Geometry.Point(310, 350);
    var p16 = new Geometry.Point(250, 350);
    var line7 = new Geometry.Line(p15, p16, context1);
    line7.draw("grey");
    rect1.fill(value1, "blue");
    if (this.value1 == 170) {
        line1.draw("green");
        line5.draw("grey");
        line6.draw("grey");
        line7.draw("grey");
    }
    else {
        line1.draw("grey");
        line5.draw("green");
        line6.draw("green");
        line7.draw("green");
    }
}
//for second tank
function tank2() {
    var t1 = new Geometry.Point(520, 120);
    var rect3 = new Geometry.Rectangle(context1, 170, 120, t1);
    rect3.draw();
    //draw small square
    var t2 = new Geometry.Point(500, 200);
    var rect4 = new Geometry.Rectangle(context1, 20, 20, t2);
    rect4.draw();
    //draw line
    var t3 = new Geometry.Point(500, 210);
    var t4 = new Geometry.Point(350, 210);
    var line_A1 = new Geometry.Line(t3, t4, context1);
    line_A1.draw("grey");
    //draw bending lines
    var t5 = new Geometry.Point(350, 210);
    var t6 = new Geometry.Point(350, 300);
    var line_A2 = new Geometry.Line(t5, t6, context1);
    line_A2.draw("grey");
    var t7 = new Geometry.Point(350, 300);
    var t8 = new Geometry.Point(250, 300);
    var line_A3 = new Geometry.Line(t7, t8, context1);
    line_A3.draw("grey");
    rect3.fill(value2, "purple");
    if (this.value2 == 170) {
        line_A1.draw("green");
        line_A2.draw("green");
        line_A3.draw("green");
    }
    else {
        line_A1.draw("grey");
        line_A2.draw("grey");
        line_A3.draw("grey");
    }
}
var c1 = new Geometry.Point(100, 330);
var a1 = new Geometry.Point(80, 330);
var C = new Geometry.circle(c1, a1, 5, context1);
//FOR and gate
function gates() {
    //draw line for and gate
    var t9 = new Geometry.Point(250, 375);
    var t10 = new Geometry.Point(250, 275);
    var line_A4 = new Geometry.Line(t9, t10, context1);
    line_A4.draw("black");
    var t11 = new Geometry.Point(250, 375);
    var t12 = new Geometry.Point(200, 375);
    var line_A5 = new Geometry.Line(t11, t12, context1);
    line_A5.draw("black");
    var t13 = new Geometry.Point(250, 275);
    var t14 = new Geometry.Point(200, 275);
    var line_A6 = new Geometry.Line(t13, t14, context1);
    line_A6.draw("black");
    //curve 
    context1.beginPath();
    context1.arc(200, 325, 50, -(Math.PI / 2), Math.PI / 2, true);
    context1.strokeStyle = 'black';
    context1.lineWidth = 3;
    context1.stroke();
    //draw line
    var t15 = new Geometry.Point(100, 330);
    var t16 = new Geometry.Point(150, 330);
    var line_A7 = new Geometry.Line(t15, t16, context1);
    if (this.value1 != 170 && this.value2 == 170)
        line_A7.draw("green");
    else
        line_A7.draw("grey");
    //Draw big circle
    var c3 = new Geometry.Point(80, 330);
    var a3 = new Geometry.Point(80, 330);
    var C3 = new Geometry.circle(c3, a3, 30, context1);
    C3.draw();
    //Draw small circle
}
c1 = new Geometry.Point(100, 330);
a1 = new Geometry.Point(80, 330);
C = new Geometry.circle(c1, a1, 5, context1);
//for first tank animation
function start() {
    this.value1 = parseInt(document.getElementById("tank1").value);
    this.value2 = parseInt(document.getElementById("tank2").value);
    animation();
}
function animation() {
    tank1();
    tank2();
    gates();
    if (this.value2 == 170 && this.value1 != 170) {
        context1.fillStyle = "red";
        C.draw();
        C.updateangle();
        console.log(this.value1);
        this.value1++;
        window.requestAnimationFrame(animation);
    }
}
//# sourceMappingURL=app.js.map