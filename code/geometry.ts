namespace Geometry{


    export class Point{
        public x: number;
        public y: number;

        constructor(x: number,y: number)
        {
            this.x =x;
            this.y=y;

        }
    }
    export class Rectangle
    {
        private _l: number;
        private _w: number;
        private stpoint1:Point;
        public color:string;
        public context:CanvasRenderingContext2D;
        
        

        constructor(context: CanvasRenderingContext2D,l:number,w:number,start:Point,color?:string){
            this._l = l;
            this._w = w;
            this.stpoint1 = start;
            this.context = context;
            
            if(typeof color!='undefined')
            this.color = color;
            else
            this.color = 'white';
            
        }
    
    

    draw()
    {
        
        this.context.beginPath();
        this.context.rect(this.stpoint1.x,this.stpoint1.y,this._w,this._l);
        this.context.fillStyle=this.color;
        this.context.fill();
        this.context.stroke();
        
        
    }

    fill(length:number,color:string){
        this.context.fillStyle = color;
        this.context.fillRect(this.stpoint1.x,this.stpoint1.y,120,length);//to fill rectangle keep width(150)as it
    }
    
  }

  //class to draw line
  export class Line
    {
        public p1: Point;
        public p2: Point;
        public context: CanvasRenderingContext2D;
        
        constructor(p1: Point,p2: Point,context: CanvasRenderingContext2D)
        {
            this.p1=p1;
            this.p2=p2;
            this.context = context;
           
        }
        draw(color:string)
        {
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y);
            this.context.lineTo(this.p2.x,this.p2.y);
            this.context.lineWidth=4;
            this.context.strokeStyle=color;
            this.context.stroke();
            this.context.strokeStyle="black";
            this.context.lineWidth=2;
        }
        
    }

    //class circle
    
    export class Init
    {
        //public radius:number;
        public x: number;
        public y: number;
        constructor(x:number,y:number)
        {
            this.x=x;
            this.y=y;
            //this.radius=radius;

        }

    }

  

    //circle class
    
    export class circle {
        public centre: Point;
        public arbtpt: Point;
        public r:number;
        public angle:number;
        public length:number;
        public context:any;
        public color: any;
        constructor (centre:Point , arbtpt:Point , r:number , context: CanvasRenderingContext2D)
        {
            this.centre=centre;
           
            this.arbtpt=arbtpt;
            this.r=r;
            this.getangle();
            this.context = context;
            this.color = "black";
            this.getlength();
        }
        getangle ()
        {
            this.angle=Math.atan((this.centre.y-this.arbtpt.y)/(this.centre.x-this.arbtpt.x));
            this.angle=this.angle*180/Math.PI;
            
        }
        draw() {
            
            this.context.beginPath();
            this.context.arc(this.centre.x, this.centre.y, this.r, 0, 2 * Math.PI, false);
            this.context.lineWidth=3;
            this.context.color="yellow";
            this.context.fillStyle="white";
            this.context.fill();
            this.context.stroke();
    
        }
        updateangle ()
        {
          
            this.angle=this.angle+5;
            this.centre.x = this.arbtpt.x + this.length*Math.cos(this.angle*Math.PI/180);
            this.centre.y = this.arbtpt.y + this.length*Math.sin(this.angle*Math.PI/180);
            
        }
        getlength() 
        {
            this.length = Math.pow((this.arbtpt.x-this.centre.x),2)+Math.pow((this.arbtpt.y-this.centre.y),2);
            this.length = Math.sqrt(this.length);
        }
    }
}